#! /bin/pyhton3

import re
import binascii
from bitstring import BitArray
from itertools import chain

class commandParser:
	# Parameters:
	# theFile: 	the file to be parsed
	# tag1:		primary tag; starts a new sequence of commands etc;
	# 			used as identifier for the dictonary. " ", "," ";" "." 
	#			can be used as seperator. If tag1 is none, the all lines
	#			without '#' and some hex number inside is interpreted
	#			as one input stream.
	# tag2:		this is expected to be in the same line as tag1. If
	#			tag2 is not None, this expression is _required_ in 
	#			the line of tag1 and the result after "=" is used as
	#			additional informaiton. (seperators like above)
	#			tag1 needs to be not None if tag2 is used.
	def __init__(self, theFile, tag1="MsgID", tag2=None):
		if tag1==None and tag2!=None:
			print("if tag2 is not None, tag1 needs to be not None")
			raise ValueError
		self.__tag1=tag1
		self.__tag2=tag2
		self.__resultList=[]
		self.__theIdx=0
		
		
		with open(theFile, 'r') as f:
			lastTag=None
			if tag1 != None:
				if tag2==None:
					tagString="("+tag1+"[^=]*=[ ]*)(?P<tag1>[^ ,;.\n\t\r]*)"
				else:
					tagString=tag1+"[^=]*=[ ]*(?P<tag1>[^ ,;.\n\t\r]*).*"+tag2+"[ ]*=[ ]*(?P<tag2>[^ ,;.\n\t\r]*)"
					
			commandString="[ \t\r]*[^#=]*=*[ \t\r]*(?P<command>[0-9a-fA-F]+)";
			
			for line in f:
				if tag1!=None:
					mTag=re.search(tagString, line)
					if mTag:
						if tag2!=None:
							self.__resultList.append([mTag.group("tag1"),mTag.group("tag2"),bytearray()])
						else:
							self.__resultList.append([mTag.group("tag1"),bytearray()])
						lastTag=mTag.group("tag1")
				else:
					self.__resultList.append([bytearray()])
				if lastTag!=None or tag1==None:
					mCommand=re.match(commandString, line)
					if mCommand:
						binCommand = binascii.unhexlify(mCommand.group("command"))
						self.__resultList[-1][-1]=self.__resultList[-1][-1]+bytearray(BitArray(bytes=binCommand).bytes)
		if self.__resultList==[]:
			print("No data or tags found while parsing "+theFile)
			raise RuntimeError
		
	def __iter__(self, method='all'):
		# iterates over parsed data
		# method can be 'all', 't1', 't2', 'd', 't1&t2', 't1&d', 't2&d'
		# 'all': 	returns values of [tag1, tag2, data] 
		#		 	tag1, tag2 is not returned if it was set to None
		#			upon object instantiation
		# 't1':	 	returns values of tag1 if tag1 if tag1 was not None 
		#			upon object instantiation
		# 't2':	 	returns balue of tag2 if tag2 was not None
		#			upon object instantiation
		# 'd':	 	returns data for the current element
		# 't1&t2':	returns values of tag1, tag2 if both argumets are 
		#			not none upon object instantiation 
		# 't1&d':	returns values of [tag1, data] if tag1 is not None
		#			upon object instantiation. 
		# 't2&d':	returns values of [tag2, data] if tag2 is not None
		#			upon object instantiation. 
		# for any other case, iterator is stopped.
		if method=='all':
			for line in self.__resultList:
				yield line
		elif (method=='t1') and self.__tag1!=None:
			for line in self.__resultList:
				yield line[0]
		elif (method=='t2') and self.__tag2!=None:
			for line in self.__resultList:
				yield line[1]
		elif (method=='d'):
			for line in self.__resultList:
				yield line[2-int(tag2==None)-int(tag1==None)]
		elif (method=='t1&t2') and self.__tag1!=None and self.__tag2!=None:
			for line in self.__resultList:
				yield [line[0], line[1]]
		elif (method=='t1&d') and self.__tag1!=None and self.__tag2!=None:
			for line in self.__resultList:
				yield [line[0], line[2]]
		elif (method=='t1&d') and self.__tag1!=None and self.__tag2==None:
			for line in self.__resultList:
				yield [line[0], line[1]]
		elif (method=='t2&d') and self.__tag1!=None and self.__tag2!=None:
			for line in self.__resultList:
				yield [line[1], line[2]]
		else:
			raise StopIteration()
	
	def getAll(self):
		return self.__resultList
	
	def __next__(self):
		self.__theIdx=self.__theIdx+1
		if self.__theIdx<=len(self.__resultList):
			return self.__resultList[self.__theIdx-1]
		else:
			raise StopIteration()
            
if __name__=="__main__":
	testObject1=commandParser("./TV_256_32/pdi.txt");
	testObject2=commandParser("./TV_256_32/pdi.txt", tag2="KeyID");
	
	for item in testObject1:
		print(item)
	
	
	for item in testObject2.__iter__('t2&d'):
		print(item)
