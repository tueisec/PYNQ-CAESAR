#!/usr/bin/env python3.6
from pynq import Overlay
from pynq.drivers import DMA
from pynq import MMIO
import time
import commandParser as parser
import binascii
import csv



CSV_NAME = "dummy1.csv"
KAT_PATH = "../software/aeadtvgen/KAT/dummy1/"
ol = Overlay("../bitstreams/dummy1.bit")
ol.download()

ol.ip_dict

# create and configure aead-axil-slave 
aead_addr = ol.ip_dict["SEG_AXIS_CAESAR_CORE_0_S00_AXI_CONFIG_reg"][0]
aead_addr_range = ol.ip_dict["SEG_AXIS_CAESAR_CORE_0_S00_AXI_CONFIG_reg"][1]
aead = MMIO(aead_addr,aead_addr_range)

######## no need to change things below! ######

# set address offset for aead status and config registers
aead_config_reg0 = 0  # status: pdi count 
aead_config_reg1 = 4  # status: sdi count
aead_config_reg2 = 8  # status: pdo count
aead_config_reg3 = 12 # config: last count


#set addres for dma_pdi
dma_pdi_addr = ol.ip_dict["SEG_axi_dma_pdi_Reg"][0]
# create pdi_dma and configure it as read from mem, write to stream
dma_pdi = DMA(dma_pdi_addr, 0)  # 'DMA_TO_DEV'
# create dma_pdi_buffer of size 14336 bytes
dma_pdi.create_buf(14336)
# create buffer object: write to this buffer 32 bit values
pdi_buffer = dma_pdi.get_buf(32)


# set address for dma_sdi
dma_sdi_addr = ol.ip_dict["SEG_axi_dma_sdi_Reg"][0]
# create sdi_dma and configure it as read from mem, write to stream
dma_sdi = DMA(dma_sdi_addr, 0)  # 'DMA_TO_DEV'
# create dma_sdi buffer of size 14336 bytes
dma_sdi.create_buf(14336)
# create buffer object: write to this buffer 32 bit values
sdi_buffer = dma_sdi.get_buf(32)


# set address for dma_pdo
dma_pdo_addr = ol.ip_dict["SEG_axi_dma_pdo_Reg"][0]
# create pdo_dma and configure it as write to mem, read from stream
dma_pdo = DMA(dma_pdo_addr, 1) # 'DMA_FROM_DEV'
#create dma_pdo_buffer of size 14336 bytes
dma_pdo.create_buf(14336)
# create buffer object: read from this buffer 32 bit values
pdo_buffer = dma_pdo.get_buf(32)

# set address for xilinx timer 0
timer_0_addr = ol.ip_dict["SEG_axi_timer_0_Reg"][0]
timer_0_addr_range = ol.ip_dict["SEG_axi_timer_0_Reg"][1]
# create xilinx timer 0
timer_0 = MMIO(timer_0_addr, timer_0_addr_range)

# set address for xilinx timer 1
timer_1_addr = ol.ip_dict["SEG_axi_timer_1_Reg"][0]
timer_1_addr_range = ol.ip_dict["SEG_axi_timer_1_Reg"][1]
# create xilinx timer 1
timer_1 = MMIO(timer_1_addr, timer_1_addr_range)

# define address offset of control and status regs
timer_TCSR0 = 0x0 # control and status
timer_TLR0 = 0x04 #load reg
timer_TCR0 = 0x08 #counter reg
timer_TCSR1 = 0x10 #control and status
timer_TLR1 = 0x14 # load reg
timer_TCR1 = 0x18 # coutner reg
#configure timer_0:
timer_0.write(timer_TCSR0, 0x00000409) # 0 1 0 0 0 0 0 0 1 0 0 1: Enables all, enables external capure, capture mode
timer_0.write(timer_TCSR1, 0x00000409)

#configure timer_1:
timer_1.write(timer_TCSR0, 0x00000409) # 0 1 0 0 0 0 0 0 1 0 0 1: Enables all, enables external capure, capture mode
timer_1.write(timer_TCSR1, 0x00000409)


#def print_dma_status():

	#print("Read from Memory, Write to FIFO")
	
	#dma_pdi_s = MMIO(dma_pdi_addr, 128)
	#print("MM 2 Stream        Ctrl   : " + format(dma_pdi_s.read(0x0), '02x'))
	#print("Binary                    : " + format(dma_pdi_s.read(0x0), '0b'))
	#print("MM 2 Stream        Status : " + format(dma_pdi_s.read(0x4), '02x'))
	#print("Binary                    : " + format(dma_pdi_s.read(0x4), '0b'))

	#dma_sdi_s = MMIO(dma_sdi_addr, 128)
	#print("\nMM 2 Stream      Ctrl   : " + format(dma_sdi_s.read(0x0), '02x'))
	#print("Binary                    : " + format(dma_sdi_s.read(0x0), '0b'))
	#print("MM 2 Stream        Status : " + format(dma_sdi_s.read(0x4), '02x'))
	#print("Binary                    : " + format(dma_sdi_s.read(0x4), '0b'))
	
	#print("\nRead from FIFO, Write to Memory")
	
	#dma_pdo_s = MMIO(dma_pdo_addr, 128)
	#print("Stream to MM       Ctrl   : " + format(dma_pdo_s.read(0x30), '02x'))
	#print("Binary                    : " + format(dma_pdo_s.read(0x30), '0b'))
	#print("Stream to MM       Status : " + format(dma_pdo_s.read(0x34), '02x'))
	#print("Binary                    : " + format(dma_pdo_s.read(0x34), '0b'))

#def dma_reset_irq():
	#control = dma_pdi_s.read(0x4)
	#control = control | 0x1000
	#dma_pdi_s.write(0x4, control)
	
	#control = dma_sdi_s.read(0x4)
	#control = control | 0x1000
	#dma_sdi_s.write(0x4, control)
	
	#control = dma_pdo_s.read(0x34)
	#control = control | 0x1000
	#dma_pdo_s.write(0x34, control)
 


#read data from KAT files and provide iterator
sdi_data=parser.commandParser(KAT_PATH+"sdi.txt", tag2="KeyID")
sdiIter=iter(sdi_data)
pdi_data=parser.commandParser(KAT_PATH+"pdi.txt", tag2="KeyID")
pdo_data=parser.commandParser(KAT_PATH+"do.txt")
pdoIter=iter(pdo_data)


execution_cycles_timer_0=0
execution_cycles_timer_1=0
execution_time_timer_0 = 0
execution_time_timer_1 = 0

pdi_count = 0
sdi_count = 0
pdo_count = 0
error_count = 0		
# Test all vectors from KAT and write results to CSV

with open(CSV_NAME,'a') as csvfile:
	# configure csv
	writer=csv.writer(csvfile,delimiter=';')
	writer.writerow(['dummy1','Msg','AEAD(us)','AEAD with FIFO(us)','Python(us)','Status'])
	
	#send and receive data
	sdiTag2=None
	
    ############# getting next PDI ############# 
	
	for [pdiTag1, pdiTag2, pdi] in pdi_data:
		
        ############# getting next PDO ############# 

		pdo=next(pdo_data)[1]		
		
        ############# SDI Transfer ############# 

		# if keyTags differ, get new sdi vector from file and send it
		if not(pdiTag2 == sdiTag2):
			[sdiTag1, sdiTag2, sdi]=next(sdi_data)
			for i in range(0, int(len(sdi)/4)): # /4: Due to our buffer configuration we always write 32 bits! 
				sdi_buffer[i] = int.from_bytes(sdi[4*i:4*i+3+1], byteorder='big', signed=False) # copy data in sdi-dma-buffer
			dma_sdi.transfer(len(sdi), 0) # 0 => direction: from PS to PL
			sdi_transmitted = aead.read(aead_config_reg1) - sdi_count
			if (sdi_transmitted != len(sdi)/4): # #set _tlast to expected amount of WORDS #FIXME for different SIZES
				print("Error during key transfer!")
				print("Only ", sdi_transmitted, "of", len(sdi)/4, "segments of the key were transmitted!")
				exit(1)

        ############# filling software DMA buffer ############# 

		print("Testing msg: ",pdiTag1, " secret: ",pdiTag2, end='  ')
		for i in range(0, int(len(pdi)/4)):#/4 Due to our buffer configuration we always write 32 bits!
			pdi_buffer[i] = int.from_bytes(pdi[4*i : 4*i+3+1], byteorder='big', signed=False)	# copy data in pdi-dma-buffer
		
		
        ############# preconfigure TLAST-generation of AEAD ############# 
			
		aead.write(aead_config_reg3,int(len(pdo)/4)) #set _tlast to expected amount of WORDS #FIXME for different SIZES
		
        ############# actual DMA transfer ############# 

		start=time.time() # start time
		# the transfer(lenght, directen)-method is not intuitive:
		# It does not mean "execute the transfer", but "be ready for executing"
		# Achtually this method CONFIGURES the DMA and if there are data present the dma starts the transfer
		dma_pdo.transfer(len(pdo), 1) # FIRST configure pdo-dma to be ready to transmit len(pdo) BYTES from the PL to the PS (1)
		dma_pdi.transfer(len(pdi), 0) # THEN  configure pdi-dma to transmit len(pdi) BYTES from the PS to the PL (0)
		end=time.time()	 # stop time

        ############# collecting additional information ############# 

		# read the two counters from timer 0. The difference is the time needed
		execution_cycles_timer_0 = timer_0.read(timer_TLR1)-timer_0.read(timer_TLR0)
		# read the two counters from timer 1. The difference is the time needed
		execution_cycles_timer_1 = timer_1.read(timer_TLR1)-timer_1.read(timer_TLR0)
		
		
		periode = 10 #ns 
		#convert from cycles to ns
		execution_time_timer_0 = execution_cycles_timer_0 * periode
		execution_time_timer_1 = execution_cycles_timer_1 * periode
		
		# read aead debugging values
		pdi_transmitted = aead.read(aead_config_reg0) - pdi_count
		pdo_transmitted = aead.read(aead_config_reg2) - pdo_count
		
		pdi_count += pdi_transmitted
		sdi_count += sdi_transmitted
		pdo_count += pdo_transmitted

        ############# evaluating results ############# 

		status = "OK"	
		## checks are sortied arcording to their severity from low (value missmatch) to high (input stall)
		
		#check if received output matches expected output
		for i in range(0,int(len(pdo)/4)): # /4: Due to our buffer configuration we always write 32 bits! 
			if (pdo_buffer[i] != int.from_bytes(pdo[4*i : 4*i+3+1], byteorder='big', signed=False)):
				status = "Missmatch"
				break;
									
		# check for valid timing
		if (execution_cycles_timer_0 < 0) or (execution_cycles_timer_1 < 0):
			status="Timer Error"
		
		# check for missmatch in number of expected and number of received/sent segments
		# an i/o stall always leads to an timer error
		if (pdo_transmitted != len(pdo)/4):
			status = "Output Stall"
			
		if (pdi_transmitted != len(pdi)/4):
				#an input stall always leads to an output stall
			status = "Input Stall"
		
	
		if (status != "OK"):
			# only print data output if there was an error
			print("not passed!")
			error_count +=1
			for i in range(0,int(len(pdo)/4)): # /4: Due to our buffer configuration we always write 32 bits! 
				print("received: ", format(pdo_buffer[i], '02x'))
				print("expected: ", format(int.from_bytes(pdo[4*i : 4*i+3+1], byteorder='big', signed=False), '02x'))
				
			print("Status is: ", status)	
			print("Number of PDI-segments transfered: ", pdi_transmitted)
			print("Number of SDI-segments transfered: ", sdi_transmitted)
			print("Number of PDO-segments transfered: ", pdo_transmitted)
			print("TLAST was set to: ", aead.read(aead_config_reg3))
				
		else:
			print("passed!")
		
		
		#only write any statistics if at least the dma transfers were completed:
		if (status == "OK") or (status == "Missmatch"):
			# print result in cycles and us
			print("Execution time (AEAD with FIFO): ", execution_cycles_timer_0 , "cycles ", execution_time_timer_0/1000, "us")
			print("Execution time (only AEAD):      ", execution_cycles_timer_1 , "cycles ", execution_time_timer_1/1000, "us")
			print("Python time:                     ",(end-start)*1000000.0, "us")	
			# write result to csv file	
			writer.writerow(["",pdiTag1,execution_time_timer_1/1000,execution_time_timer_0/1000,(end-start)*1000000,status])
		else:
			# write basically a new line to csv, but without values due to errors
			writer.writerow(["",pdiTag1,"","","",status])	
		print("Testing msg: ",pdiTag1, " secret: ",pdiTag2," done!\n")

print("\n\nFinal report:\n")

print("Total number of PDI-segments transfered: ", pdi_count)
print("Total number of SDI-segments transfered: ", sdi_count)
print("Total number of PDO-segments transfered: ", pdo_count)
print("Total number of errors: ", error_count)
exit(0)
