#!/bin/sh

max=10 # numbers of itterations



if [ "$(id -u)" -ne 0 ]; then
   echo "This script must be run as sudo" 1>&2
   exit 1
fi

for i in $(seq 0 $max)

do
 echo  "starting round $i of $max \n"
# python3.6 ./dummy1_pynq_v1.4.py > /dev/null
 python3.6 ./dummy1_pynq_v2.0_legacyDMA.py > /dev/null
# python3.6 ./dummy1_pynq_v2.0_newDMA.py > /dev/null
done

exit 0
