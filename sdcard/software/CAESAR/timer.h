#include <sys/time.h>
#include <stdio.h> 
#define TIMER 1
struct timeval  start_time_ex, stop_time_ex;
double          start, stop, seconds, useconds;

/* --------------------------------------------------------------------- */
/* start the timer for exponantiation                                    */
/* --------------------------------------------------------------------- */
void timer_start_ex()
{
    if (gettimeofday(&start_time_ex, (struct timezone *) 0) <0)
        perror("t_start: gettimeofday ");
}         

/* --------------------------------------------------------------------- */
/* stop the timerfor exponantiation                                      */
/* --------------------------------------------------------------------- */
void timer_stop_ex()
{
    if (gettimeofday(&stop_time_ex, (struct timezone *) 0) <0)
        perror("t_stop: gettimeofday ");
}

/* --------------------------------------------------------------------- */
/* return elapsed time for multiplication                                */
/* --------------------------------------------------------------------- */
double timer_get_ex()
{
    start = ((double) start_time_ex.tv_sec) * 1000000.0 + start_time_ex.tv_usec;
    stop  = ((double) stop_time_ex.tv_sec)  * 1000000.0 + stop_time_ex.tv_usec;
    //seconds = (stop - start) / 1000000.0;
    useconds = (stop - start);
    //return(seconds);
    return(useconds);
}

