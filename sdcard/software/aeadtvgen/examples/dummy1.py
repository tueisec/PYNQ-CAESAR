#!/usr/bin/env python3
# -*- coding: utf-8 -*

import os
import sys


try:
    from aeadtvgen import aeadtvgen
except ImportError:
    print("================================================================")
    print("Warning: aeadtvgen not installed!!!! Use source package instead.")
    print("================================================================")
    sys.path.append(os.path.abspath('..'))
    from aeadtvgen import aeadtvgen

if __name__ == '__main__':
    # ========================================================================
    # Create the list of arguments for aeadtvgen
    args = [
        os.path.realpath('../../lib'),  # Library path
        'dummy1',                       # Library name
        '--io', '32', '32',             # I/O width: PDI/DO and SDI width, respectively.
        '--key_size', '128',            # Key size
        '--npub_size', '96',            # Npub size
        '--nsec_size', '0',             # Nsec size
        '--tag_size', '128',            # Tag size
        '--block_size',    '128',       # Data block size
        '--block_size_ad', '128',       # AD block size
        # '--ciph_exp',                   # Ciphertext expansion
        # '--add_partial',                # ciph_exp option: add partial bit
        # '--ciph_exp_noext',             # ciph_exp option: no block extension when a message is a multiple of a block size
        # '--offline',                    # Offline cipher (Adds Length segment as the first input segment)
#        '--dest', '../../../hardware/dummy1/scripts',   # destination folder
        '--dest', '../KAT/dummy1',   # destination folder
        '--max_ad', '80',               # Maximum random AD size
        '--max_d', '80',                # Maximum random message size
        '--max_io_per_line', '8',       # Max number of w-bit I/O word per line
        '--human_readable',             # Generate a human readable text file
       # '--verify_lib',                 # Verify reference enc/dec in reference code
                                        # Note: (This option performs decryption for
                                        #        each encryption operation used to
                                        #        create the test vector)
    ]
    # ========================================================================
    # Alternative way of creating an option argument
    #   message format
    msg_format = '--msg_format npub ad data tag'.split()
    #   Run routine (add at least one to args array)
    #
    #   Note: All the encrypt and decrypt operation specifiers refer to the 
    #   input test vector  for hardware core. The actual process in the 
    #   preparation of the test vector is based only on the encryption operation.
    gen_test_routine = '--gen_test_routine 1 20 2'.split()
    gen_random = '--gen_random 3'.split()
    gen_custom = ['--gen_custom',   # Note: Use this format!!
        '''\
        True,   False,      0,          0:
		1,      0,          8,          8:
		1,      0,          16,        16:
        1,      0,          32,        32:
        1,      0,          64,        64:
        1,      0,          128,        128:
        1,      0,          256,        256:
		1,      0,          512,        512:
        1,      0,          1024,        1024:
        1,      0,          1024,        2048
        ''']
    gen_single = ['--gen_single',
        '1',                                # Encrypt(0)/decrypt(1)
        '000102030405060708090A0B0C0D0E0F', #Key
        '000102030405060708090A0B0C0D0E0F', #Npub
        '000102030405060708090A0B0C0D0E0F', #Nsec (Ignored: nsec_size=0)
        '000102030405060708090A0B0C0D0E0F', #AD
        '000102030405060708090A0B0C0D0E0F', #DATA
        ]
    # ========================================================================
    # Add option arguments together
    args += msg_format
    args += gen_custom
    # ========================================================================
    # Pass help flag through
    try:
        if (sys.argv[1] == '-h'):
            args.append('-h')
    except:
        pass
    # Call program
    aeadtvgen(args)
