#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pkg_resources import get_distribution, DistributionNotFound


__project__ = 'aeadtvgen'
__author__ = 'Ekawat (Ice) Homsirikamol'
__package__ = 'aeadtvgen'

try:
    __version__ = get_distribution(__project__).version    
except DistributionNotFound:
    __version__ = '(N/A - Local package)'

from .cli import *

    
