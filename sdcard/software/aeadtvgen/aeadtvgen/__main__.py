#!/usr/bin/env python3
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import sys
    from .cli import aeadtvgen
    aeadtvgen(sys.argv[1:])
