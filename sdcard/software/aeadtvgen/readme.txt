===========
Compilation
===========
> python -m pip install --upgrade pip
> python -m pip install --upgrade setuptools
> python -m pip install wheel
> python setup.py bdist_wheel
A distribution wheel (*.whl) will be created in /dist folder


============
Installation
============
User can install the python program using pip by:
> cd dist
> python -m pip install aeadtvgen-{__version__}-py3-none-any.whl


============
Usage
============

See help
> python -m aeadtvgen -h

============
Example
============

See examples/ for more details