library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.AEAD_pkg.all;

entity axis_aead_wrapper is
	generic (
		-- Users to add parameters here

	G_W      : integer :=32;    --! Public data input
    G_SW	 : integer := 32;

	-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI_CONFIG
		C_S00_AXI_CONFIG_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_CONFIG_ADDR_WIDTH	: integer	:= 4


	);
	port (
		-- Users to add ports here

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI_CONFIG
		s00_axi_config_aclk	: in std_logic;
		s00_axi_config_aresetn	: in std_logic;
		s00_axi_config_awaddr	: in std_logic_vector(C_S00_AXI_CONFIG_ADDR_WIDTH-1 downto 0);
		s00_axi_config_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_config_awvalid	: in std_logic;
		s00_axi_config_awready	: out std_logic;
		s00_axi_config_wdata	: in std_logic_vector(C_S00_AXI_CONFIG_DATA_WIDTH-1 downto 0);
		s00_axi_config_wstrb	: in std_logic_vector((C_S00_AXI_CONFIG_DATA_WIDTH/8)-1 downto 0);
		s00_axi_config_wvalid	: in std_logic;
		s00_axi_config_wready	: out std_logic;
		s00_axi_config_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_config_bvalid	: out std_logic;
		s00_axi_config_bready	: in std_logic;
		s00_axi_config_araddr	: in std_logic_vector(C_S00_AXI_CONFIG_ADDR_WIDTH-1 downto 0);
		s00_axi_config_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_config_arvalid	: in std_logic;
		s00_axi_config_arready	: out std_logic;
		s00_axi_config_rdata	: out std_logic_vector(C_S00_AXI_CONFIG_DATA_WIDTH-1 downto 0);
		s00_axi_config_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_config_rvalid	: out std_logic;
		s00_axi_config_rready	: in std_logic;

		-- Ports of Axi Slave Bus Interface S00_AXIS_PDI
		s00_axis_pdi_aclk	: in std_logic;
		s00_axis_pdi_aresetn	: in std_logic;
		s00_axis_pdi_tready	: out std_logic;
		s00_axis_pdi_tdata	: in std_logic_vector(G_W-1 downto 0);
		s00_axis_pdi_tlast	: in std_logic;
		s00_axis_pdi_tvalid	: in std_logic;

		-- Ports of Axi Slave Bus Interface S01_AXIS_SDI
		s01_axis_sdi_aclk	: in std_logic;
		s01_axis_sdi_aresetn	: in std_logic;
		s01_axis_sdi_tready	: out std_logic;
		s01_axis_sdi_tdata	: in std_logic_vector(G_SW-1 downto 0);
		s01_axis_sdi_tlast	: in std_logic;
		s01_axis_sdi_tvalid	: in std_logic;

		-- Ports of Axi Master Bus Interface M00_AXIS_PDO
		m00_axis_pdo_aclk	: in std_logic;
		m00_axis_pdo_aresetn	: in std_logic;
		m00_axis_pdo_tvalid	: out std_logic;
		m00_axis_pdo_tdata	: out std_logic_vector(G_W-1 downto 0);
		m00_axis_pdo_tstrb	: out std_logic_vector((G_W/8)-1 downto 0);
		m00_axis_pdo_tlast	: out std_logic;
		m00_axis_pdo_tready	: in std_logic
	);
end axis_aead_wrapper;

architecture arch_imp of axis_aead_wrapper is

constant C_PDI_WIDTH : integer := G_W;
constant C_SDI_WIDTH : integer := G_SW;
constant C_PDO_WIDTH : integer := G_W;
		-- Parameters of Axi Slave Bus Interface S00_AXIS_PDI
constant		C_S00_AXIS_PDI_TDATA_WIDTH	: integer	:= G_W;

		-- Parameters of Axi Slave Bus Interface S01_AXIS_SDI
constant		C_S01_AXIS_SDI_TDATA_WIDTH	: integer	:= G_SW;

		-- Parameters of Axi Master Bus Interface M00_AXIS_PDO
constant		C_M00_AXIS_PDO_TDATA_WIDTH	: integer	:= G_W;
    
	-- component declaration
	component S00_AXI_CONFIG is
		generic (
		C_S_AXI_DATA_WIDTH	: integer;
		C_S_AXI_ADDR_WIDTH	: integer
		);
		port (
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic;
        config_reg0_r : out std_logic_vector (31 downto 0);
        config_reg1_r : out std_logic_vector (31 downto 0);
        config_reg2_r : out std_logic_vector (31 downto 0);
        config_reg3_r : out std_logic_vector (31 downto 0);
        
        config_reg0_w : in std_logic_vector (31 downto 0);
        config_reg1_w : in std_logic_vector (31 downto 0);
        config_reg2_w : in std_logic_vector (31 downto 0);
        config_reg3_w : in std_logic_vector (31 downto 0);
        
        config_reg0_w_en : in std_logic;
        config_reg1_w_en : in std_logic;
        config_reg2_w_en : in std_logic;
        config_reg3_w_en : in std_logic
        
		);
	end component S00_AXI_CONFIG;

	component S00_AXIS_PDI is
		generic (
		C_S_AXIS_TDATA_WIDTH	: integer;
		C_PDI_WIDTH : integer
		);
		port (
		S_AXIS_ACLK	: in std_logic;
        S_AXIS_ARESETN    : in std_logic;
        S_AXIS_TREADY    : out std_logic;
        S_AXIS_TDATA    : in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
        S_AXIS_TLAST    : in std_logic;
        S_AXIS_TVALID    : in std_logic;
        pdi             : out std_logic_vector(C_PDI_WIDTH-1 downto 0);
        pdi_valid       : out std_logic;
        pdi_ready       : in  std_logic 
		);
	end component S00_AXIS_PDI;

	component S01_AXIS_SDI is
            generic (
            C_S_AXIS_TDATA_WIDTH    : integer;
            C_SDI_WIDTH : integer 
            );
            port (
            S_AXIS_ACLK    : in std_logic;
            S_AXIS_ARESETN    : in std_logic;
            S_AXIS_TREADY    : out std_logic;
            S_AXIS_TDATA    : in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
            S_AXIS_TLAST    : in std_logic;
            S_AXIS_TVALID    : in std_logic;
            sdi             : out std_logic_vector(C_SDI_WIDTH-1 downto 0);
            sdi_valid       : out std_logic;
            sdi_ready       : in std_logic
            );
	end component S01_AXIS_SDI;

	component M00_AXIS_PDO is
		generic (
		C_M_AXIS_TDATA_WIDTH	: integer;
        C_PDO_WIDTH : integer
        );
        port (
        M_AXIS_ACLK    : in std_logic;
        M_AXIS_ARESETN    : in std_logic;
        M_AXIS_TVALID    : out std_logic;
        M_AXIS_TDATA    : out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
        M_AXIS_TSTRB    : out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
        M_AXIS_TLAST    : out std_logic;
        M_AXIS_TREADY    : in std_logic;
        pdo             : in std_logic_vector (C_PDO_WIDTH-1 downto 0);
        pdo_valid       : in std_logic;
        pdo_ready       : out std_logic;
        pdo_last        : in std_logic;
        t_last_counter_max : in integer
		);
	end component M00_AXIS_PDO;
	
	component AEAD is
        generic (
            --! I/O size (bits)
            G_W             : integer;    --! Public data input
            G_SW            : integer    --! Secret data input
        );
        port (
            --! Global ports
            clk             : in  std_logic;
            rst             : in  std_logic;
            --! Publica data ports
            pdi_data        : in  std_logic_vector(G_W              -1 downto 0);
            pdi_valid       : in  std_logic;
            pdi_ready       : out std_logic;
            --! Secret data ports
            sdi_data        : in  std_logic_vector(G_SW             -1 downto 0);
            sdi_valid       : in  std_logic;
            sdi_ready       : out std_logic;
            --! Data out ports
            do_data         : out std_logic_vector(G_W              -1 downto 0);
            do_ready        : in  std_logic;
            do_valid        : out std_logic
           -- do_last         : out std_logic
        );
    end component AEAD;

    signal aead_config_reg0 : std_logic_vector (31 downto 0);
    signal aead_config_reg1 : std_logic_vector (31 downto 0);
    signal aead_status_0 :  std_logic_vector (31 downto 0);
    signal aead_status_0_en :  std_logic;
    signal aead_status_1 :  std_logic_vector (31 downto 0);
    signal aead_status_1_en :  std_logic;
	

    signal stream_out : std_logic_vector (31 downto 0);
    
    signal t_last_counter_max : integer;
    
    signal pdi : std_logic_vector (C_PDI_WIDTH-1 downto 0);
    signal pdi_valid : std_logic;
    signal pdi_ready : std_logic;
    
    signal sdi : std_logic_vector (C_SDI_WIDTH-1 downto 0);
    signal sdi_valid : std_logic;
    signal sdi_ready : std_logic;
    
    signal pdo : std_logic_vector (C_PDO_WIDTH-1 downto 0);
    signal pdo_valid : std_logic;
    signal pdo_ready : std_logic;
    signal pdo_last  : std_logic;
    
    signal rst : std_logic;
    signal clk : std_logic;
    
    signal  config_reg0_w :std_logic_vector (31 downto 0);
    signal  config_reg1_w :std_logic_vector (31 downto 0);
    signal  config_reg2_w :std_logic_vector (31 downto 0);
    signal  config_reg3_w :std_logic_vector (31 downto 0);
    
    signal config_reg0_w_en : std_logic;
    signal config_reg1_w_en : std_logic;
    signal config_reg2_w_en : std_logic;
    signal config_reg3_w_en : std_logic;
    
    signal  config_reg0_r :std_logic_vector (31 downto 0);
    signal  config_reg1_r :std_logic_vector (31 downto 0);
    signal  config_reg2_r :std_logic_vector (31 downto 0);
    signal  config_reg3_r :std_logic_vector (31 downto 0);
    
    signal pdi_counter : integer;
    signal pdo_counter : integer;
    signal sdi_counter : integer;
    
begin

-- Instantiation of Axi Bus Interface S00_AXI_CONFIG
config_port : S00_AXI_CONFIG
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_CONFIG_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_CONFIG_ADDR_WIDTH
	)
	port map (
		S_AXI_ACLK	=> s00_axi_config_aclk,
		S_AXI_ARESETN	=> s00_axi_config_aresetn,
		S_AXI_AWADDR	=> s00_axi_config_awaddr,
		S_AXI_AWPROT	=> s00_axi_config_awprot,
		S_AXI_AWVALID	=> s00_axi_config_awvalid,
		S_AXI_AWREADY	=> s00_axi_config_awready,
		S_AXI_WDATA	=> s00_axi_config_wdata,
		S_AXI_WSTRB	=> s00_axi_config_wstrb,
		S_AXI_WVALID	=> s00_axi_config_wvalid,
		S_AXI_WREADY	=> s00_axi_config_wready,
		S_AXI_BRESP	=> s00_axi_config_bresp,
		S_AXI_BVALID	=> s00_axi_config_bvalid,
		S_AXI_BREADY	=> s00_axi_config_bready,
		S_AXI_ARADDR	=> s00_axi_config_araddr,
		S_AXI_ARPROT	=> s00_axi_config_arprot,
		S_AXI_ARVALID	=> s00_axi_config_arvalid,
		S_AXI_ARREADY	=> s00_axi_config_arready,
		S_AXI_RDATA	=> s00_axi_config_rdata,
		S_AXI_RRESP	=> s00_axi_config_rresp,
		S_AXI_RVALID	=> s00_axi_config_rvalid,
		S_AXI_RREADY	=> s00_axi_config_rready,
		
        config_reg0_r  => config_reg0_r,
        config_reg1_r  => config_reg1_r,
        config_reg2_r  => config_reg2_r,
        config_reg3_r  => config_reg3_r,
        
        config_reg0_w => config_reg0_w,
        config_reg1_w => config_reg1_w,
        config_reg2_w => config_reg2_w,
        config_reg3_w => config_reg3_w,
        
        config_reg0_w_en => config_reg0_w_en,
        config_reg1_w_en => config_reg1_w_en,
        config_reg2_w_en => config_reg2_w_en,
        config_reg3_w_en => config_reg3_w_en
	);

-- Instantiation of Axi Bus Interface S00_AXIS_PDI
PDI_port : S00_AXIS_PDI
	generic map (
		C_S_AXIS_TDATA_WIDTH	=> C_S00_AXIS_PDI_TDATA_WIDTH,
		C_PDI_WIDTH             => C_PDI_WIDTH
	)
	port map (
		S_AXIS_ACLK   	=> s00_axis_pdi_aclk,
		S_AXIS_ARESETN	=> s00_axis_pdi_aresetn,
		S_AXIS_TREADY	=> s00_axis_pdi_tready,
		S_AXIS_TDATA	=> s00_axis_pdi_tdata,
		S_AXIS_TLAST	=> s00_axis_pdi_tlast,
		S_AXIS_TVALID	=> s00_axis_pdi_tvalid,
		pdi             => pdi,
		pdi_valid       => pdi_valid,
		pdi_ready       => pdi_ready
	);

-- Instantiation of Axi Bus Interface S01_AXIS_SDI
SDI_port : S01_AXIS_SDI
	generic map (
		C_S_AXIS_TDATA_WIDTH	=> C_S01_AXIS_SDI_TDATA_WIDTH,
		C_SDI_WIDTH             => C_SDI_WIDTH
	)
	port map (
		S_AXIS_ACLK	    => s01_axis_sdi_aclk,
		S_AXIS_ARESETN	=> s01_axis_sdi_aresetn,
		S_AXIS_TREADY	=> s01_axis_sdi_tready,
		S_AXIS_TDATA	=> s01_axis_sdi_tdata,
		S_AXIS_TLAST	=> s01_axis_sdi_tlast,
		S_AXIS_TVALID	=> s01_axis_sdi_tvalid,
		sdi             => sdi,
		sdi_valid       => sdi_valid,
		sdi_ready       => sdi_ready
	);

-- Instantiation of Axi Bus Interface M00_AXIS_PDO
PDO_port : M00_AXIS_PDO
	generic map (
		C_M_AXIS_TDATA_WIDTH	=> C_M00_AXIS_PDO_TDATA_WIDTH,
		C_PDO_WIDTH => C_PDO_WIDTH
	)
	port map (
		M_AXIS_ACLK	=> m00_axis_pdo_aclk,
		M_AXIS_ARESETN	=> m00_axis_pdo_aresetn,
		M_AXIS_TVALID	=> m00_axis_pdo_tvalid,
		M_AXIS_TDATA	=> m00_axis_pdo_tdata,
		M_AXIS_TSTRB	=> m00_axis_pdo_tstrb,
		M_AXIS_TLAST	=> m00_axis_pdo_tlast,
		M_AXIS_TREADY	=> m00_axis_pdo_tready,
		pdo             => pdo,
        pdo_valid       => pdo_valid,
        pdo_ready       => pdo_ready,
        pdo_last        => pdo_last,
        t_last_counter_max => t_last_counter_max
	);
	
	
	t_last_counter_max <= to_integer(unsigned(config_reg3_r));

	-- Add user logic here
  --  pdo <= stream_out when aead_config_reg0 (0) = '0' else
  --                    NOT stream_out;
                      
  --  sdi_ready   <= pdo_ready when aead_config_reg0 (1) = '1' else '0';
  --  pdi_ready   <= pdo_ready when aead_config_reg0 (1) = '0' else '0';
  --  pdo_valid   <= pdi_valid when aead_config_reg0 (1) = '0' else sdi_valid;
  --  stream_out  <= pdi       when aead_config_reg0 (1) = '0' else sdi;
   
   
   
CAESAR_API:  AEAD
      generic map (
	    G_W => G_W,
            G_SW => G_SW
    )
      port map(
          --! Global ports
          clk             => clk,
          rst             => rst,
          --! Publica data ports
          pdi_data        => pdi,
          pdi_valid       => pdi_valid,
          pdi_ready       => pdi_ready,
          --! Secret data ports
          sdi_data        => sdi,
          sdi_valid       => sdi_valid,
          sdi_ready       => sdi_ready,
          --! Data out ports
          do_data        => pdo,
          do_ready       => pdo_ready,
          do_valid       => pdo_valid
         -- do_last        => pdo_last
      );
   
   -- selct PDO clk and rst for AEAD
    clk <= m00_axis_pdo_aclk;
    rst <= NOT m00_axis_pdo_aresetn;
    
  process (clk)
  begin
    if (rising_edge (clk)) then
        if (rst = '1') then
            pdi_counter <= 0;
            sdi_counter <= 0;
            pdo_counter <= 0;
        else
            if (pdo_ready = '1' and pdo_valid = '1') then
                pdo_counter <= pdo_counter +1;
            end if;
            
            if (pdi_ready = '1' and pdi_valid = '1') then
                pdi_counter <= pdi_counter +1;
            end if;
            
            if (sdi_ready = '1' and sdi_valid = '1') then
                sdi_counter <= sdi_counter +1;
            end if;          
        end if;              
    end if;
  end process;
  
  config_reg0_w <=  std_logic_vector(to_unsigned(pdi_counter, config_reg0_w'length));
  config_reg1_w <=  std_logic_vector(to_unsigned(sdi_counter, config_reg1_w'length));
  config_reg2_w <=  std_logic_vector(to_unsigned(pdo_counter, config_reg2_w'length));
  
  config_reg0_w_en <= '1';
  config_reg1_w_en <= '1';
  config_reg2_w_en <= '1';
 

  

	-- User logic ends

end arch_imp;
