library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity S00_AXIS_PDI is
	generic (
		-- Users to add parameters here
        C_PDI_WIDTH : integer;
		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- AXI4Stream sink: Data Width
		C_S_AXIS_TDATA_WIDTH	: integer
	);
	port (
		-- Users to add ports here
		pdi : out std_logic_vector (C_PDI_WIDTH-1 downto 0); 
        pdi_ready : in std_logic;
        pdi_valid : out std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- AXI4Stream sink: Clock
		S_AXIS_ACLK	: in std_logic;
		-- AXI4Stream sink: Reset
		S_AXIS_ARESETN	: in std_logic;
		-- Ready to accept data in
		S_AXIS_TREADY	: out std_logic;
		-- Data in
		S_AXIS_TDATA	: in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
		-- Indicates boundary of last packet
		S_AXIS_TLAST	: in std_logic;
		-- Data is in valid
		S_AXIS_TVALID	: in std_logic
	);
end S00_AXIS_PDI;

architecture arch_imp of S00_AXIS_PDI is

begin	
	pdi <= S_AXIS_TDATA; 
    pdi_valid <= S_AXIS_TVALID;
    S_AXIS_TREADY <= pdi_ready;

end arch_imp;
