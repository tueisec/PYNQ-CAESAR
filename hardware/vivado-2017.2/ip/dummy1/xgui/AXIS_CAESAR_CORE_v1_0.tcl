# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "C_S00_AXI_CONFIG_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_CONFIG_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "G_SW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "G_W" -parent ${Page_0}


}

proc update_PARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_CONFIG_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_CONFIG_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_CONFIG_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_CONFIG_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.G_SW { PARAM_VALUE.G_SW } {
	# Procedure called to update G_SW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.G_SW { PARAM_VALUE.G_SW } {
	# Procedure called to validate G_SW
	return true
}

proc update_PARAM_VALUE.G_W { PARAM_VALUE.G_W } {
	# Procedure called to update G_W when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.G_W { PARAM_VALUE.G_W } {
	# Procedure called to validate G_W
	return true
}


proc update_MODELPARAM_VALUE.G_W { MODELPARAM_VALUE.G_W PARAM_VALUE.G_W } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.G_W}] ${MODELPARAM_VALUE.G_W}
}

proc update_MODELPARAM_VALUE.G_SW { MODELPARAM_VALUE.G_SW PARAM_VALUE.G_SW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.G_SW}] ${MODELPARAM_VALUE.G_SW}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH PARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CONFIG_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_CONFIG_ADDR_WIDTH}
}

