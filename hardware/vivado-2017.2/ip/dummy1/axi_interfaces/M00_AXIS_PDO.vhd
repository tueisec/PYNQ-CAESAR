library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity M00_AXIS_PDO is
	generic (
		-- Users to add parameters here
        C_PDO_WIDTH : integer;
     	-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
		C_M_AXIS_TDATA_WIDTH	: integer

	);
	port (
		-- Users to add ports here
        pdo : in std_logic_vector (C_PDO_WIDTH-1 downto 0);
        pdo_valid : in std_logic;
        pdo_ready : out std_logic;
        pdo_last  : in std_logic;
        t_last_counter_max : in integer;
        
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global ports
		M_AXIS_ACLK	: in std_logic;
		-- 
		M_AXIS_ARESETN	: in std_logic;
		-- Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted. 
		M_AXIS_TVALID	: out std_logic;
		-- TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
		M_AXIS_TDATA	: out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
		-- TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
		M_AXIS_TSTRB	: out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
		-- TLAST indicates the boundary of a packet.
		M_AXIS_TLAST	: out std_logic;
		-- TREADY indicates that the slave can accept a transfer in the current cycle.
		M_AXIS_TREADY	: in std_logic
	);
end M00_AXIS_PDO;

architecture implementation of M00_AXIS_PDO is

signal tlast_counter : integer;

begin
	M_AXIS_TVALID	<= pdo_valid;
	M_AXIS_TDATA	<= pdo;
	M_AXIS_TSTRB	<= (others => '1');
    M_AXIS_TLAST	<= '1' when tlast_counter = t_last_counter_max-1 else '0';
    --M_AXIS_TLAST    <= pdo_last;
    pdo_ready       <= M_AXIS_TREADY;
    
	
	tlast_generation: process(M_AXIS_ACLK) 
	begin
	if (rising_edge(M_AXIS_ACLK)) then
	   if(M_AXIS_ARESETN = '0') then
	       tlast_counter <= 0;
	   elsif (pdo_valid = '1' and M_AXIS_TREADY = '1') then -- there is a transaction. Be careful when combining ready and valid signals!
	       if (tlast_counter = t_last_counter_max-1) then
	           tlast_counter <= 0;
	       else
	           tlast_counter <= tlast_Counter +1;
	       end if;
	   else
	       tlast_counter <= tlast_counter;
	   end if;
    end if;
    end process tlast_generation; 

end implementation;
