#!/bin/bash

##### usage #####
## generates tcl files for project setup
## and synthesis from a block design created by
## "write_bd_tcl design_name.tcl"
## the output of this script can be piped into a
## makefile

##### configuration ## ###
## either specify a particular block design
## or multiple block designs

BD_FILES=./bd/*.tcl
#BD_FILES=./bd/dummy1.tcl

##### don't change things below #####

echo -e "# auto-generated makefile commands #\n"

# loop through all specified tcl files
for INPUT_TCLFILE in $BD_FILES
do

BD=${INPUT_TCLFILE%.tcl} # remove suffix (file extention)
PROJECT_NAME=${BD##*/} # remove prefix (leading paths)

PROJECT_DIR="$PROJECT_NAME" # name used by vivado
TCLFILE_SETUP="setup/${PROJECT_NAME}_setup.tcl" # generates the vivado project
TCLFILE_SYNTH="synth/${PROJECT_NAME}_synth.tcl" # synthesizes the vivado project

VIVADO="vivado -mode batch -source" # name and options of vivado used in the makefile
TCL_PATH="../../tcl" # relative path to this folder used by in the makefile


### I. _setup.tcl ###

# 1) add header to setup tcl file
# this specifies the platform (-part) and the location of the ips
cat > ${TCLFILE_SETUP} <<EOF
create_project -force ${PROJECT_NAME} ${PROJECT_DIR}   -part xc7z020clg400-1

set_property ip_repo_paths  ../ip [current_project]
update_ip_catalog
EOF

# 2) append the block design from the bd file
cat ${INPUT_TCLFILE} >> ${TCLFILE_SETUP}

# 3) append trailer 
# this specifies the constraints (xdc) and toplevel (top.v)
# additionally one can comment in the commands for synthesis
cat >> ${TCLFILE_SETUP} <<EOF
add_files -fileset constrs_1 -norecurse ./src/constraints/top.xdc

add_files -norecurse ./src/top.v
set_property top top [current_fileset]
update_compile_order -fileset sources_1


## call implement
## uncomment to synthesis the design as well
#launch_runs impl_1 -to_step write_bitstream -jobs 8
#wait_on_run impl_1

## move and rename bitstream to final location
#file copy -force ./${PROJECT_DIR}/${PROJECT_NAME}.runs/impl_1/pynq_caesar_wrapper.bit ../../bitstream/pynq_${PROJECT_NAME}.bit

## This hwardware definition file will be used for microblaze projects
## but we don't have any microblazes so far
#file mkdir ./${PROJECT_DIR}/${PROJECT_NAME}.sdk
#write_hwdef -force  -file ./${PROJECT_DIR}/${PROJECT_NAME}.sdk/${PROJECT_NAME}.hdf
#file copy -force ./${PROJECT_DIR}/${PROJECT_NAME}.sdk/${PROJECT_NAME}.hdf ../../sdk/

EOF

### II. dedicated _synth.tcl script

cat > ${TCLFILE_SYNTH} << EOF

open_project ./${PROJECT_DIR}/${PROJECT_NAME}.xpr

## call implement
launch_runs impl_1 -to_step write_bitstream -jobs 8
wait_on_run impl_1

## move and rename bitstream to final location
file copy -force ./${PROJECT_DIR}/${PROJECT_NAME}.runs/impl_1/pynq_caesar_wrapper.bit ../../bitstream/${PROJECT_NAME}.bit

## This hwardware definition file will be used for microblaze projects
## but be don't have any microblazes so far
#file mkdir ./${PROJECT_DIR}/${PROJECT_NAME}.sdk
#write_hwdef -force  -file ./${PROJECT_DIR}/${PROJECT_NAME}.sdk/${PROJECT_NAME}.hdf
#file copy -force ./${PROJECT_DIR}/${PROJECT_NAME}.sdk/${PROJECT_NAME}.hdf ../../sdk/

# writes block design and copies it to the bd dir
open_bd_design ./${PROJECT_DIR}/${PROJECT_NAME}.srcs/sources_1/bd/pynq_caesar/pynq_caesar.bd
write_bd_tcl -force ../../tcl/bd/${PROJECT_NAME}.tcl
close_bd_design [current_bd_design]
EOF

### output for makefile

echo -e "### ${PROJECT_NAME} ###"
echo -e "${PROJECT_NAME}_setup:\n\t${VIVADO} ${TCL_PATH}/${TCLFILE_SETUP}"
#echo -e "${PROJECT_NAME}_synth:\n\t${VIVADO} ${TCL_PATH}/${TCLFILE_SYNTH}"
#echo -e "with make dependencies:\n"
echo -e "${PROJECT_NAME}_synth: ${PROJECT_NAME}_setup\n\t${VIVADO} ${TCL_PATH}/${TCLFILE_SYNTH}"
echo -e "${PROJECT_NAME}_clean:\n\trm -rf ${PROJECT_NAME}\n"

done #end for loop

## append make clean to makefile
echo -e "### removes vivado log and jou files ###"
echo -e "clean:\n\trm *.log *.jou\n"
## append make package_ip to makefile
echo -e "### package ip ###"
echo -e "package_ip:\n\t${VIVADO} ${TCL_PATH}/ip/package_ip.tcl\n"

