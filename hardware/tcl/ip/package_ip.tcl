#Vendor name and description
set VENDOR_NAME_DISPLAYED  "Technical University of Munich"
set VENDOR_NAME "TUM"
set VENDOR_URL "https://www.sec.ei.tum.de"
set IP_CORE_NAME_DISPLAYED "dummy1"
set IP_CORE_DESCRIPTION "This is to demonstrate the generated result of our script"
set IP_CORE_REVISION 1

#change name here!
#set SRC_PATH "../ip/template"
set SRC_PATH "../ip/dummy1"

set VENDOR "user.org"
set LIBRARY "user"


# tempory project for IP-Core generation
set PROJECT_NAME "my_aead"
set PROJECT_PATH "/tmp"

### don't change things below! ###
#set IP_REPO "../ip"
set TOP_LEVEL "axis_aead_wrapper"

#this name is used to connect the core in the bd
set IP_CORE_NAME "AXIS_CAESAR_CORE"

ipx::infer_core -vendor $VENDOR -library $LIBRARY -taxonomy /CAESAR $SRC_PATH
ipx::edit_ip_in_project -upgrade true -name $PROJECT_NAME -directory $PROJECT_PATH/$PROJECT_NAME.tmp $SRC_PATH/component.xml
ipx::current_core $SRC_PATH/component.xml
update_compile_order -fileset sources_1
set_property vendor $VENDOR_NAME [ipx::current_core]
set_property name $IP_CORE_NAME [ipx::current_core]
set_property display_name $IP_CORE_NAME_DISPLAYED [ipx::current_core]
set_property description $IP_CORE_DESCRIPTION [ipx::current_core]
set_property vendor_display_name $VENDOR_NAME_DISPLAYED [ipx::current_core]
set_property company_url $VENDOR_URL [ipx::current_core]
set_property previous_version_for_upgrade $VENDOR:$LIBRARY:$TOP_LEVEL:1.0 [ipx::current_core]
set_property core_revision $IP_CORE_REVISION [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
close_project -delete
set_property ip_repo_paths [concat [get_property ip_repo_paths [current_project]] $SRC_PATH] [current_project]
update_ip_catalog -rebuild
#update_ip_catalog -rebuild -repo_path $IP_REPO
ipx::unload_core $SRC_PATH/component.xml
