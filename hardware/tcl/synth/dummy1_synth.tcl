
open_project ./dummy1/dummy1.xpr

## call implement
launch_runs impl_1 -to_step write_bitstream -jobs 8
wait_on_run impl_1

## move and rename bitstream to final location
file copy -force ./dummy1/dummy1.runs/impl_1/pynq_caesar_wrapper.bit ../../bitstream/dummy1.bit

## This hwardware definition file will be used for microblaze projects
## but be don't have any microblazes so far
#file mkdir ./dummy1/dummy1.sdk
#write_hwdef -force  -file ./dummy1/dummy1.sdk/dummy1.hdf
#file copy -force ./dummy1/dummy1.sdk/dummy1.hdf ../../sdk/

# writes block design and copies it to the bd dir
open_bd_design ./dummy1/dummy1.srcs/sources_1/bd/pynq_caesar/pynq_caesar.bd
write_bd_tcl -force ../../tcl/bd/dummy1.tcl
close_bd_design [current_bd_design]
