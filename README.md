# Build a Design

1. `$ cd ./hardware/vivado-2017.2/pynq-caesar`
2. `$ make <cipher>_setup ` to only create vivado project
3. `$ make <cipher>_synth ` to create the vivado project, the bitfile and block design TCL

# Test a Design:
Prepare the files on the local machine:

1. `$ cp ./hardware/bitstream/cipher>.bit ./sdcard/bitstreams/`
2. `$ cp ./hardware/tcl/bd/<cipher>.tcl ./sdcard/bitstreams/` 
3. `$ scp -r ./sdcard xilinx@<pnyq-ip>`

Run them on PYNQ:

1. `$ ssh xilinx@<pnyq-ip>`
2. enable designs in `~/sdcard/python/batch.sh` 
3. `$ sudo ~/sdcard/python/batch.sh` 
4. Results can be found in `./sdcard/python/<cipher>.csv`



# Create your own Design:
0. starting with the provided template: `cp ./hardware/vivado-2017.2/ip/template ./hardware/vivado-2017.2/ip/<your_design>`
1. add your CipherCore implenetation to `hardware/vivado-2017.2/ip/<your_design>/ciphercore`
2. generate an IP-Core with the provided tcl script
  * adjust the variables in `./hardware/tcl/ip/package_ip.tcl`
  * `$ cd ./hardware/vivado-2017.2/pynq-caesar`
  * `$ make package_ip`
3. Create a block design using vivado (best is to start with an existing design or the provided dummy1.xpr (`make dummy1_setup`)

# Integrate your Desing into the Design Flow:
4. use the `write_bd_tcl <your-design>.tcl` command in vivado to create a tcl representation of your block design.
5. Copy `<your-design>.tcl`to `./hardware/tcl/bd/`
6. Execute`./hardware/tcl/create_tcl.sh > Makefile`
7. Copy new Makefile to `./hardware/vivado-2017.2/pynq-caesar`
8. Now you can regenerate `<your-design>` using `make <your-design>_setup`
